'''
Created on Nov 19, 2017

@author: Ryan Xocoyotlzin Hernandez
'''
import os
import wfdb
from IPython.display import display

class DatabasePhysionet:
    '''
    classdocs
    '''    

    def __init__(self, dbname='mimic2db', initDir=None):
        '''
        Constructor
        '''
        if initDir is None:
            initDir = os.environ['PHYSIONET_DB']
            if not os.path.exists(initDir):
                initDir = os.getcwd()
                        
        self.dbname = dbname           
        self.path = os.path.join(initDir, dbname)
        self.__DownloadData__()
    
    def __DownloadData__(self):
                
        # If the data already exists, return.
        if os.path.exists(self.path):
            return 
        
        print("Downloading MIT data...")
        # Download all the WFDB content
         
        wfdb.dldatabase(self.dbname, dlbasedir = self.path, numRecords=1000)
        
        # Display the downloaded content in the folder
        display(os.listdir(self.path)[0])
        print("Download Successful")
        
    def GetRecord(self, recordName):
        recordDir = os.path.join(self.path, recordName)
        '''
        Temporary hack to deal with a known bug in the current 
        iteration of wfdb.
        '''
        recordDir = os.path.join(recordDir, recordName)
        return wfdb.rdsamp(recordDir)
        
if __name__ == '__main__':
    
    databasePhysionet = DatabasePhysionet(initDir = r'\\WIN-9FGFTLTCEA8\engineering\Database\Biomed\MIT')
    
    recordName = 'a40001'
    record = databasePhysionet.GetRecord(recordName)
    wfdb.plotrec(record, title='Record ' + recordName) 
