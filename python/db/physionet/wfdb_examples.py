'''
Created on Nov 19, 2017

@author: Ryan Xocoyotlzin Hernandez
'''
import wfdb
from IPython.display import display

if __name__ == '__main__':
    
    dbs = wfdb.getdblist()
    display([dname for dname in dbs if 'ahadb' in dname[0].lower()])