'''
Created on Nov 23, 2017

@author: Ryan Xocoyotlzin Hernandez
'''

class Heart(object):
    '''
    Heart object to be used in the synthesis of signals. 
    '''
    
    """
    According to this article:
    
        Mouse Heart Rate in a Human: Diagnostic Mystery of an Extreme 
        Tachyarrhythmia. Lovely Chhabra et. al.
    
    the max recorded heart rate is 600 BPM. However, the "accepted" theoretical 
    bound seems to be about 300 BPM(from the same article):
    
        With normal cardiac physiology, it is known that following the action 
        potential (AP), the absolute refractory period (ARP) prevents another AP
        until the channels are reset at the AV Junction. The ARP of the AV Node 
        lasts about 0.2 seconds limiting the heart rate to 300/min in theory. 
        Following the ARP is the relative refractory period (RRP), and t
        he stimulus needs to be greater than before, as the original electrical 
        potential across the membrane hasn't been fully restored. So, 300 beats 
        per minute is not sustainable for long, as the stimulus needs to be 
        progressively greater each time to generate the next AP. Another 
        possible factor is the cardiac myocyte action potential duration which 
        is normally about 200 msec which again theoretically would limit the 
        heart rate to about 300 beats per minute. Heart rate conduction above 
        300 beats per minute would thus involve the presence of at least a 
        bypass tract, shorter cardiac myocyte AP duration and also probable 
        selective cardiac myocyte activation.
        
    
    """
    MAX_THEORETICAL_HEART_RATE = 300.0
    MAX_EXPERIMENTAL_HEART_RATE = 600.0
    
    MIN_THEORETICAL_HEART_RATE  = 20.0
    


    def __init__(self, params):
        '''
        Constructor
        '''
        pass