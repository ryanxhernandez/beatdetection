'''
Created on Dec 6, 2017

@author: Ryan Xocoyotlzin Hernandez
'''
class BeatDetector(object):
    '''
    classdocs
    '''

    
    def __init__(self, detectionAlg, observers=[]):
        '''
        Constructor
        '''
        self.detectionAlg = detectionAlg
        self.observers = observers
        
    def __call__(self, data):
        self.detectionAlg.AddData(data)
        for classification, beat in self.detectionAlg:
            self.UpdateObservers(beat, classification)
            yield classification, beat
    
    def UpdateObservers(self, signal, classification):
        for observer in self.observers:
            observer.OnSignalClassified(signal, classification)
    
    def Register(self, observer):
        self.observers.append(observer)
    
    def Unregister(self, observer):
        self.observers.append(observer)
    
    def UnregisterAll(self):
        unregistrationSuccess = True
        for observer in self.observers:
            unregistrationSuccess &= self.Unregister(observer) 
        return unregistrationSuccess       