'''
Created on Dec 6, 2017

@author: Ryan Xocoyotlzin Hernandez
'''
import numpy as np
import matplotlib.pyplot as plt

class PlottingObserver:
    
    def __init__(self, accum=False, title=None):
        self.accum = accum
        
        if self.accum:
            self.outputClassifications = []
            self.outputSignal = []
        self.x = None
        self.title = title
        
    def OnSignalClassified(self, signal, classification):
        
        if self.title is not None:
            plt.title(self.title)
        if self.accum:
            self.outputSignal += signal
            if classification:
                self.outputClassifications += np.ones(len(signal)).tolist()
            else:
                self.outputClassifications += np.zeros(len(signal)).tolist()
                
        else:
            self.RealtimePlot(signal, classification)
            
    def RealtimePlot(self, signal, classification):
        if self.x is None:
            self.x = np.linspace(0,len(signal)-1,len(signal))
        elif len(self.x) != len(signal):
            lastX = self.x[-1]
            self.x = np.linspace(lastX,lastX+len(signal)-1,len(signal))
        else:
            self.x += len(signal)
        
        if classification > 0:
            marker = '1'
            label = 'Valid Blood Pressure Signal'
        else:
            marker = '*'
            label = 'Invalid Blood Pressure Signal'
        #if classification:
        plt.plot(self.x, signal,marker=marker,label=label) 
            
    def AccumPlot(self):
        if not self.accum:
            raise ValueError("Accum must be set to True before sending data" 
                             "to this observer. Please rerun data with accum"
                             "set to True.")
        self.outputSignal = np.array(self.outputSignal)
        self.outputClassifications = np.array(self.outputClassifications)
        
        badIdcs = np.where(self.outputClassifications ==0)[0]
        goodIdcs = np.where(self.outputClassifications==1)[0]
        x = np.linspace(1,len(self.outputSignal),len(self.outputSignal))
        plt.scatter(x[goodIdcs],self.outputSignal[goodIdcs], 
                        marker='*', label='Valid Blood Pressure Signal')
        plt.scatter(x[badIdcs],self.outputSignal[badIdcs], 
                    marker='1',label='Invalid Blood Pressure Signal')
        