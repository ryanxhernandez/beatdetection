'''
Created on Nov 19, 2017

@author: Ryan Xocoyotlzin Hernandez
'''

class Dissector(object):
    '''
    classdocs
    '''


    def __init__(self, sections, xdata, ydata):
        '''
        Constructor
        '''
        self.sections = sections
        self.xdata = xdata
        self.ydata = ydata
        self.xout = []
        self.yout = []
        
    
    def dissect(self):
        for section in self.sections:
            start = section[0] 
            for subidx in section[1:]:
                stop = subidx
                if self.xdata is not None:
                    self.xout.append( self.xdata[start:stop] )
                self.yout.append( self.ydata[start:stop] )
                start = stop
                    
    def __iter__(self):
        if self.xdata is not None:
            for x, y in zip(self.xout, self.yout):
                yield x, y
        else:
            for y in self.yout:
                yield y
    
    def __len__(self):
        return len(self.yout)
    
    def tolist(self):
        return self.yout
    
if __name__ == '__main__':
    import numpy as np
    import matplotlib.pyplot as plt
    
    def GetSincExampleData():
        f = 100.0
        fs = 1000*f
        Ts = 1/fs
        numCrests =  20
        t = np.arange(0,numCrests/f, 1.0/(fs))
        fx = np.sin(2*np.pi*f*t)/t
        
        FindIdx = lambda pt, data, threshold : np.where(abs(data-pt)<threshold)[0][1]
        over2piF = 1/(2*np.pi*f)
        sections = []
        # start at 0
        section1 = [1]
        # Stop at the second crest 
        idx = FindIdx(over2piF*5*np.pi/2,t,1.25*Ts)
        section1.append(idx)
        
        section2 = []
        # Start at the 5th crest 
        section2.append( FindIdx(over2piF*(4*4+1)*np.pi/2,t,1.25*Ts) )
        # and stop at the 8th crest
        section2.append( FindIdx(over2piF*(4*7+1)*np.pi/2,t,1.25*Ts) )
        
        section3 = []
        # Start at the 12th crest 
        section3.append( FindIdx(over2piF*(4*11+1)*np.pi/2,t,1.25*Ts) )
        # and stop at the 19th crest
        section3.append( FindIdx(over2piF*(4*18+1)*np.pi/2,t,1.25*Ts) )
        
        sections.append(section1)
        sections.append(section2)
        sections.append(section3)
        
        return t, fx, sections
    
    t, fx, sections  = GetSincExampleData()
    dissector = Dissector(sections, t, fx)
    dissector.dissect()
    
    ax1 = plt.subplot(2,1,1)
    plt.plot(t, fx)
    plt.subplot(2,1,2, sharex=ax1)
    
    for x,y in dissector:
        plt.plot(x,y)
    plt.show()
    