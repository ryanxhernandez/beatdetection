'''
Created on Nov 26, 2017

@author: Ryan Xocoyotlzin Hernandez
'''
import itertools
import numpy as np
from collections import deque 
import matplotlib.pyplot as plt

class DynamicLengthSlidingWindow():
    
    ALG_UNTIL_UNSUCCESSFUL = 0
    ALG_SUCCESSFUL_UNTIL_UNSUCCESSFUL = 1
    ALG_TEST = 2
        
    def __init__(self, classifier, data=None, minLen=2, maxLen=10, step=1, 
                 algType = ALG_SUCCESSFUL_UNTIL_UNSUCCESSFUL):
        self.classifier = classifier
        self.step = step
        self.data = deque(maxlen=maxLen*2)
        self.buffer = deque(maxlen=maxLen)
        self.data = data
        if data is not None:
            self.dataLen = len(data)
        self.maxLen = maxLen
        self.minLen = minLen
        self.SetAlgType(algType)
        self.idx = 0
            
    def SetIdx(self, idx):
        self.idx = idx        
        
    def GetMaxClassifyLength(self):
        return self.maxLen
    
    def GetMinClassifyLength(self):
        return self.minLen

    def AddData(self, data):
        '''
        TODO: Implement s.t. you can incrementally add data. 
        self.data.extend(data)
        '''
        self.data = data
        self.dataLen = len(data)
                
    def GetData(self):
        return self.data
     
    def GetAlgType(self):
        return self.algType
    
    def SetAlgType(self, algType):
        if algType == DynamicLengthSlidingWindow.ALG_UNTIL_UNSUCCESSFUL:
            self.__FineStep = self.__FineStepUS
        elif algType == DynamicLengthSlidingWindow.ALG_TEST:
            self.__FineStep = self.__FineStepTest
        else:
            self.__FineStep = self.__FineStepSUU
    
    def __iter__(self):
        
        if self.data is None:
            raise ValueError("Data must be initialized to run algorithm!")
        
        for result, subset in self.__CoarseStep():
            yield result, subset
    
    def __CoarseStep(self):
        
        while self.__FillBuffer():    
            yield self.__FineStep(self.buffer)
                       
    def __FillBuffer(self):
        
        numValues = min( self.maxLen-len(self.buffer), self.dataLen-self.idx)
        self.buffer.extend(self.data[self.idx:self.idx+numValues])
        self.idx +=  numValues
        return len(self.buffer)
    
    def __GetFineStepParams(self, buffer):
        '''
        Abstract away the things that are likely to change.
        '''
        get = lambda data, start, end: list(itertools.islice(data, start, end))
        result = False
        start = max(self.step,self.minLen)
        stop = len(buffer)
        step = self.step
        return get, result, start, stop, step
    
    def __FineStepUS(self, buffer):
        get, result, start, stop, step = self.__GetFineStepParams(buffer)
        
        for k in range(start,stop,step):
                
            subset = get(buffer, 0, k)
            result = self.classifier(subset)
            if result > 0:
                for _ in range(len(subset)):
                    self.buffer.popleft()
                return result, subset
        
        return result, [self.buffer.popleft()]
    
    def __FineStepSUU(self, buffer):
        get, result, start, stop, step = self.__GetFineStepParams(buffer)

        prevResult = result
        prevSubSet = None
        
        for k in range(start, stop, step):
                        
            subset = get(buffer, 0, k)
            result = self.classifier(subset)
            
            # Result changed from successful to unsuccessful. Output last 
            # successful dataset.
            if not result  and prevResult:
                for _ in range(len(subset)-self.step):
                    self.buffer.popleft() 
                return 1, prevSubSet
            
            prevResult = result
            prevSubSet = subset
            
        # Nothing was found; pop off last piece of data.
        return result, [self.buffer.popleft()]
    
    def __FineStepTest(self, buffer):
        get, result, start, stop, step = self.__GetFineStepParams(buffer)

        prevResult = result
        prevSubSet = None
        
        for k in range(start, stop, step):
                        
            subset = get(buffer, 0, k)
            result = self.classifier(subset)
            
            # Result changed from successful to unsuccessful. Output last 
            # successful dataset.
            if result < 0 and prevResult > 0:
                for _ in range(len(subset)-self.step):
                    self.buffer.popleft() 
                return 1, prevSubSet
            elif result > 0 and prevResult > 0 and prevResult > result:
                for _ in range(len(subset)-self.step):
                    self.buffer.popleft() 
                return 1, prevSubSet
            
            prevResult = result
            prevSubSet = subset
            
        # Nothing was found; pop off last piece of data.
        return result, [self.buffer.popleft()]
        
    
if __name__ == '__main__':
    
    
    def Classify(data):
        from random import random, seed
        seed(42)
        result =  np.abs(data[-1][1] - .5) > 0.5#random()
        return result
        
    def GetSinData():
    
        xlen = int(1e3)
        f = 10
        x = np.linspace(1,xlen,xlen)/(2*np.pi*f)
        fx = np.sin(x)
        return np.transpose(np.array([x,fx]))
    
    def RunSinTest(dlsw):
        x = []
        y = []
        results = []
        
        for result, subset in dlsw:
            subset = np.array(subset)
            results += np.ones(len(subset)).tolist() if result else [0]
            x += subset[:,0].tolist()
            y += subset[:,1].tolist()
    
        results = np.array(results)
        x = np.array(x)
        y = np.array(y)
        
        badIdcs = np.where(results==0)[0]
        goodIdcs = np.where(results==1)[0]
        plt.figure()
        plt.scatter(x[goodIdcs],y[goodIdcs], color='green')
        plt.scatter(x[badIdcs],y[badIdcs], color='red')
        
    # TODO: Need to implement a better test to show the differences between
    #       the two DLSW algorithms.
    algType = DynamicLengthSlidingWindow
    dlsw = DynamicLengthSlidingWindow(Classify, GetSinData(), maxLen=10, 
                                      algType=algType)
    dlsw.Init()
    RunSinTest(dlsw)
    
    dlsw.SetAlg(DynamicLengthSlidingWindow.ALG_UNTIL_UNSUCCESSFUL)
    dlsw.Init()
    RunSinTest(dlsw)
    
    plt.show()
    