'''
Created on Dec 5, 2017

@author: rxher
'''
import h5py

H5_QUICK_SAVE_NAME = 'signals/x'

def QuickConvertToH5(path, x):
    f = h5py.File(path,'w')
    f.create_dataset(H5_QUICK_SAVE_NAME, x.shape, x.dtype, x)
    f.close()
    
def QuickLoadFromH5(path):
    f = h5py.File(path,'r')
    x = f[H5_QUICK_SAVE_NAME][:]
    f.close()
    return x
    
    
if __name__ == '__main__':
    import numpy as np
    import matplotlib.pyplot as plt
    
    f = 100
    t = np.arange(0,1/(2*np.pi),1e-4)
    ft = np.sin(2*np.pi*f*t)
    
    x = np.array([t,ft])
    
    path = 'test.h5'
    QuickConvertToH5('test.h5', x)
    t_s,ft_s = QuickLoadFromH5(path)
    
    plt.subplot(2,1,1)
    plt.plot(t,ft)
    plt.title('Before Saving')
    plt.subplot(2,1,2)
    plt.plot(t_s,ft_s)
    plt.title('After Saving')
    
    plt.show()