'''
Created on Nov 18, 2017

@author: Ryan Xocoyotlzin Hernandez
'''
# Common Collections Algorithms
import itertools
# Machine Learning
from sklearn.decomposition import PCA  
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from projects.biomed.beat_classification.python.svm.training import TrainingSets
# Physiology
from projects.biomed.beat_classification.python.physiology.cardiovascular.Heart import Heart
# DSP
import pywt
from scipy import interpolate
# Numerics
import numpy as np
# Plotting
import matplotlib.pyplot as plt
from projects.biomed.beat_classification.python.physiology.cardiovascular.algorithms.blood_pressure.beat_detection.Observers import PlottingObserver

class SVMBeatClassifier:
    ''' Support vector machine that classifies a signal as a valid beat or not.
    
    This classifier takes a signal that is of one dimension and extracts feature
    to be dimensionally reduced in order to classify the validity of the beat.
    Specifically the features to be extracted are the detail and approximation
    coefficients of a discrete wavelet transform and the number of times the 
    sign of the slope changes. These features are then dimensionally reduced to 
    12 dimensions through principal component analysis. These 12 dimensions are 
    then sent to a support vector machine that classifies whether or not this
    signal is a valid heart beat.
    
    Note:
        The support vector machine must be trained via the Train method. This 
        classifier will not classify signals without being trained.
    '''
    
    UNTRAINED_STRING = ("Support Vector Machine needs to be trained"  
                        "before you can use it!")
                       
    def __init__(self, fs, gamma=2e-1, C=1.25, nComponents=12, 
                 shouldTrain=True):
        '''Constructor
        Args:
            fs (float) : sampling frequency
            
            gamma (float): how far the influence of a single training example 
                           reaches, with low values meaning 'far' and high 
                           values meaning 'close'.
            
            C (float): trades off misclassification of training examples against
                       simplicity of the decision surface. A low C makes the 
                       decision surface smooth, while a high C aims at 
                       classifying all training examples correctly by giving the
                       model freedom to select more samples as support vectors.
            
            nComponents(int): number of components to decompose the features 
                                into.
            shouldTrain(bool): If true, the SVM will train in this constructor
                                else, the user will have to train it before
                                use of prediction facilities.
            
        '''
        # State of training
        self.__isTrained = False
        # Observers to post results to.
        self.observers = []
        # The support vector machine that will classify heart beats.
        self.svm = Pipeline((
                # Standardize features by removing the mean and scaling to unit 
                # variance.
                ("scaler", StandardScaler()),
                # Uses a C-Support Vector Classifier with a radial basis 
                # function kernel
                ("svm_clf", SVC(kernel="rbf", gamma=gamma, C=C))
        ))
        self.nComponents = nComponents
        # Principal component Analyzer to decompose the initial features.
        self.pca = PCA(n_components=self.nComponents)
        # Mode where the signal is extended according to the first derivatives 
        # calculated on the edges (straight line)
        self.dwtMode = pywt.Modes.smooth
        # Use Haar wavelets for decomposition
        self.waveletType = "haar"
        # Sampling Frequency
        self.fs = fs
        # Sampling Period
        self.Ts = 1/fs
        # Maximum length of data the SVM can classify. Lazily initialize later.
        self.__maxClassifyLen = -1
        # Minimum length of data the SVM can classify. Lazily initialize later.
        self.__minClassifyLen = -1
        
        if shouldTrain:
            self.Train()
            
    def Train(self, goodSet=TrainingSets.GetA40001TrainingGoodSet(), 
                    badSet=TrainingSets.GetA40001TrainingBadSet() ):     
        '''Trains the support vector machine with a good set of heart beats and
           a bad set of heart beats.
           
           Args:
               goodSet (list of lists of floats): First column contains a list
                                                   of heart beats. Second column
                                                    contains the target values 
                                                    for those heart beats.
                badSet (list of 2 lists of floats): First column contains a list
                                                   of features that are not 
                                                   heart beats. Second column
                                                    contains the target values 
                                                    for those features.
            Returns:
                None
            
            Examples:
                >>> svm = SVMBeatClassifier(fs=100)
                
                >>> badFeature1 = numpy.sin(np.arange(1,100))
                >>> badFeature2 = numpy.cos(np.arange(1,100))
                >>> badFeatures = [badFeature1, badFeature2]
                >>> badTargets = -numpy.ones(len(badFeatures))
                >>> badSet = [badFeatures , badTargets ]
                
                >>> goodSet = listOfGoodHeartBeats
                >>> goodTargets = numpy.ones(len(listOfGoodHeartBeats))
                >>> goodSet = [goodFeatures , goodTargets ]
                >>> svm.Train(goodSet, badSet)
            
        '''
        if len(goodSet) != 2:
            raise ValueError("Invalid shape. Good set must contain predictors"
                             "array and targets array. Current length is "
                             "%i."%len(goodSet))
        if len(badSet) != 2:
            raise ValueError("Invalid shape. Bad set must contain predictors"
                             "array and targets array. Current length is "
                             "%i."%len(badSet))
        print("Training support vector machine...")
        # Extract the predictors and the targets
        goodPredictors, goodTargets = goodSet
        badPredictors, badTargets = badSet
        
        self.predictors = goodPredictors + badPredictors
        self.targets = goodTargets + badTargets
        
        # Preprocess each feature
        for i, predictor in enumerate(self.predictors):
            self.predictors[i] = self.__Preprocess(predictor)
        
        # Save the mean value of the predictors to later subtract in order to 
        # give the predictor data zero mean.
        self.sigMean = np.mean(self.predictors)
        # Remove the mean from each predictor to get 
        for i, predictor in enumerate(self.predictors):
            self.predictors[i] = predictor - self.sigMean
            
        # Fit the model with the predictors
        self.pca.fit(self.predictors)        
        # Transform each predictor from self.__maxClassifyLen to a length of 
        # self.nComponents.
        for i, predictor in enumerate(self.predictors):
            self.predictors[i] = self.__RunPCA(predictor)
        # Convert to numpy array to reshape.
        self.predictors = np.array(self.predictors)
        # Reshape data in order for the SVM to fit the data to a model.
        self.predictors = self.predictors.reshape(len(self.predictors),
                                                  self.nComponents)
        # Fit the data
        self.svm.fit(self.predictors, self.targets)
        # SVM is now trained and able to do prediction. 
        self.__isTrained = True
        
    def IsTrained(self):
        '''Returns the state of SVM training
            
            Args:
                None
            
            Returns:
                bool: The training state. True if trained; false otherwise.
        
        '''
        return self.__isTrained
    
    
    def IsBeat(self, data):
        '''Classifies the data as a heart beat.
            Args:
                data (list of floats) : ABP data to check the existence of a 
                                        heart beat in.
            Returns:
                bool : True if it is a heart beat; false otherwise. 
            
            Examples:
                >>> svm = SVMBeatClassifier(fs=100)
                >>> svm.Train()         
                >>> badFeature1 = numpy.sin(np.arange(1,100))
                >>> svm.IsBeat(badFeature1)
                    False
        '''
        
        return self.__Classify(data) > 0
        
    def __Preprocess(self, signal): 
        ''' Preprocesses the signal to output features to be later decomposed.
        
            Args:
                signal (list of floats) : ABP data to preprocess
                
            Returns:
                features (list of floats) : A list of DWT approximation and 
                                            detail coefficients alongside the 
                                            number of slope changes.
        
        '''
        # If the length of the signal is not equal to the maximum classification
        # length, interpolate to make them equal.
        if len(signal) != self.GetMaxClassifyLength():
            signal = self.__RunInterpolation(signal)
        # Run a discrete wavelet transform to get the approximation and detail 
        # coefficients of the waveform.
        signalCoeff = self.__RunDWT(signal)
        # Count the number of times the sign of the slope changes 
        slopeSignChanges = np.array([self.__CalcNumSignChanges(np.gradient(signal))])
        # Concatenate the coefficients with the number of slope sign changes.
        features = np.append(signalCoeff,slopeSignChanges)
        return features
    
    def __Classify(self, signal):
        ''' Performs the preprocessing, principal component analysis and 
            the classification.
        
            Args:
                signal (list of floats) : ABP data to classify
                
            Returns:
                distance (float) : The distance from the hyperplane. Distance 
                                   with a positive sign indicates that it is a
                                   heart beat; negative sign indicates that it 
                                   is not a heart beat.
        
        '''
        preprocessedFeatures = self.__Preprocess(signal)
        reducedFeatures = self.__RunPCA(preprocessedFeatures)
        distance = self.svm.decision_function(reducedFeatures)
        return distance
    
    def __RunPCA(self, signal):
        ''' Performs  principal component analysis on a signal to reduce the 
            number of features to self.nComponents. 
        
            Args:
                signal (list of floats) : Features to dimensionally reduce.
                
            Returns:
                principal components (float) : The dimensionally reduced 
                                                features.
        
        '''
        # Remove the mean of the data to center data about the origin.
        signal = signal - self.sigMean
        return self.pca.transform(signal.reshape(1,-1))
    
    def __RunDWT(self,signal):
        ''' Performs  a discrete wavelet transform on a signal.
        
            Args:
                signal (list of floats) : Signal on which to perform the DWT. 
                
            Returns:
                coefficients (list of floats) : The approximation and detail 
                                                coefficients in a 1-D array.
        
        '''
        approxCoeff, detailCoeff = pywt.dwt(signal, self.waveletType, 
                                            self.dwtMode)
        # Concatenate the coefficients to match the format the principal 
        # component analyzer expects.
        coeff = np.append(approxCoeff, detailCoeff)
        return coeff
    
    def __CalcNumSignChanges(self, signal):
        ''' Calculates the number of times the sign of the slope of a signal 
            changes.
        
            Args:
                signal (list of floats) : Signal on which to calculate the sign
                                            changes.
                
            Returns:
                signChanges (int) : The number of times the sign changes.
        
        '''
        return len(list(itertools.groupby(signal, lambda signal: signal > 0)))
    
    def __RunInterpolation(self, signal):
        ''' Calculates the number of times the sign of the slope of a signal 
            changes.
        
            Args:
                signal (list of floats) : Signal on which to calculate the sign
                                            changes.
                
            Returns:
                interpolatedSignal (list of floats) : The interpolated signal.
        
        '''
        sigLen = len(signal)
        # Create a mapping along the time axis to the original signal.
        t = np.linspace(0,self.Ts*(sigLen-1),sigLen)
        # Create the new mapping for the interpolated signal.
        t_new = np.linspace(0,t[-1],self.GetMaxClassifyLength())
        # Interpolate 
        tck = interpolate.splrep(t, signal, s=0)
        return interpolate.splev(t_new, tck, der=0)
        
    def GetMaxClassifyLength(self):
        ''' Gets the maximum length of a signal that this classifier will 
            classify.
        
            Args:
                None
            Returns:
                maxLen (int) : The maximum length.
        
        '''
        if self.__maxClassifyLen < 0:
            minBeatsPerSecond = Heart.MIN_THEORETICAL_HEART_RATE/60 
            self.__maxClassifyLen = int(np.floor(self.fs/minBeatsPerSecond+0.5)) 
        return self.__maxClassifyLen
    
    def GetMinClassifyLength(self):
        ''' Gets the minimum length of a signal that this classifier will 
            classify.
        
            Args:
                None
            Returns:
                minLen (int) : The minimum length.
        
        '''
        if self.__minClassifyLen < 0:
            maxBeatsPerSecond = Heart.MAX_THEORETICAL_HEART_RATE/60 
            self.__minClassifyLen = int(np.floor(self.fs/maxBeatsPerSecond+0.5))
        return self.__minClassifyLen
    
    
    def PlotTraining(self):
        ''' Plots the training data relative to the hyperplane.
        
            Args:
                None
            Returns:
                None
        
        '''
        if not self.IsTrained():
            print(SVMBeatClassifier.UNTRAINED_STRING) 
            return  
        if self.nComponents != 2:
            raise ValueError("Number of components must be equal to 2 to plot"
                             "the training data w.r.t. the hyperplane.")
        x  = 4000
        self.plot_predictions(self.svm,[-x,x, -x, x])
        self.plot_dataset(self.predictors, np.array(self.targets), 
                          [-x, x, -x, x])
        plt.show(block=False)
        
    def plot_predictions(self, clf, axes):
        ''' Plots the predictions data relative to the hyperplane.
        
            Args:
                clf(SVC): The SVM being used.
                
                axes (axis): The axes to plot on.
            Returns:
                None
        
        '''
        plt.figure()
        sz = 1e2
        x0s = np.linspace(axes[0], axes[1], sz)
        x1s = np.linspace(axes[2], axes[3], sz)
        x0, x1 = np.meshgrid(x0s, x1s)
        X = np.c_[x0.ravel(), x1.ravel()]
        y_pred = clf.predict(X).reshape(x0.shape)
        y_decision = clf.decision_function(X).reshape(x0.shape)
        plt.contourf(x0, x1, y_pred, cmap=plt.cm.brg, alpha=0.2)
        plt.contourf(x0, x1, y_decision, cmap=plt.cm.brg, alpha=0.1)


    def plot_dataset(self,X, y, axes):
        plt.plot(X[:, 0][y==0], X[:, 1][y==0], "bs", 
                 label='Invalid Blood Pressure Signal')
        plt.plot(X[:, 0][y==1], X[:, 1][y==1], "g^", 
                 label='Valid Blood Pressure Signal')
        plt.axis(axes)
        plt.grid(True, which='both')
        plt.xlabel(r"$x_1$", fontsize=20)
        plt.ylabel(r"$x_2$", fontsize=20, rotation=0)
        plt.legend()
            
              
        
    def UpdateObservers(self, signal, classification):
        for observer in self.observers:
            observer.OnSignalClassified(signal, classification)
    
    def Register(self, observer):
        if not self.IsTrained():
            print(SVMBeatClassifier.UNTRAINED_STRING) 
            return False
        self.observers.append(observer)
        return True
    
    def Unregister(self, observer):
        if observer in self.observers:
            return False
        self.observers.append(observer)
        return True
    
    def UnregisterAll(self):
        unregistrationSuccess = True
        for observer in self.observers:
            unregistrationSuccess &= self.Unregister(observer) 
        return unregistrationSuccess       
            
if __name__ == '__main__':
    # Data structures
    from collections import OrderedDict
    # Data
    from projects.biomed.beat_classification.python.db.physionet.DatabasePhysionet import DatabasePhysionet
    
    def TestTrainingData(svm):
        # Make sure that at least the training data still works.
        plt.figure()
        ax = plt.subplot(2,1,1)

        predictors, targets = TrainingSets.GetA40001TrainingGoodSet()
        for predictor in predictors:
            svm(np.array(predictor)) 
        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        plt.legend(by_label.values(), by_label.keys())
        
        plt.subplot(2,1,2, sharex=ax)
        data = []
        for predictor in predictors:
            data += predictor.tolist()
        plt.plot(data)
        plt.show()
    
    def Test_a40001(svm, observer):
        # Tests the a40001 dataset.
        dbPhysionet = DatabasePhysionet()
        record = dbPhysionet.GetRecord('a40001')
        abp = record.p_signals[:,6]
        abp = abp[~np.isnan(abp)]
        step = svm.GetMaxClassifyLength()
        start = step*1000
        end = start + step*1000 #len(abp)
        plt.figure(figsize=(10.0,10.0))
        ax = plt.subplot(2,1,1)
        plt.plot(abp[start:end])
        
        ax.set_title("Raw Arterial Blood Pressure Signal")
        ax.set_xlabel('Sample (n)')
        ax.set_ylabel('Pressure (mmHg)')

        
        ax2 = plt.subplot(2,1,2, sharex=ax)

        svm(abp[start:end])
        #observer.AccumPlot()
        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        plt.legend(by_label.values(), by_label.keys())
        ax2.set_title("Beat Detected Arterial Blood Pressure Signal")
        ax2.set_xlabel('Sample (n)')
        ax2.set_ylabel('Pressure (mmHg)')
        plt.savefig('sig.png',dpi=500)
        plt.show()
        
    
    gamma = 2e-1
    C = 1.25
    nComponents = 2
        
    title = 'gamma=%.2f, C=%.2f, nComponents=%i'%(gamma,C,nComponents)
    observer = PlottingObserver(accum=False, title=title)
    sqiSvm = SVMBeatClassifier(fs=125.0, gamma=gamma, C=C, 
                               nComponents=nComponents)
    sqiSvm.Train()
    if observer is not None:
        sqiSvm.Register(observer)
    sqiSvm.PlotTraining()       
    plt.show()