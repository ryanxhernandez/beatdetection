'''
Created on Nov 19, 2017

@author: Ryan Xocoyotlzin Hernandez
'''
from projects.biomed.beat_classification.python.db.physionet.DatabasePhysionet import DatabasePhysionet
from projects.biomed.beat_classification.python.signals.Dissect import Dissector

import numpy as np

def CheckTrainingSetPlots(trainingSet):
    yfs = []
    for subset in trainingSet:
        plt.figure()
        #plt.subplot(2,1,1)
        plt.plot(subset)
        #plt.subplot(2,1,2)
        #xf, yf = GetFft(subset)
        ##plt.plot(xf,yf)
        #yfs.append(yf.tolist())
    
    #yfs =  np.array(yfs).reshape(len(yfs),len(xf))
    #plt.figure()
    #plt.plot(xf, np.average(yfs, axis=0))
    #plt.title('Average FFT')
        
    plt.show()
    
def GetA40001TrainingGoodSet():
    sections = []
    dbPhysionet = DatabasePhysionet()
    record = dbPhysionet.GetRecord('a40001')
    abp = record.p_signals[:,6]
    section1 = [770254,770300]
    section2 = [716643,716746]
    section3 = [569979,570086,570193,570297,570406,570509,570618]
    section4 = [770932,771051]
    section5 = [582530,582649,582769,582844]
    section6 = [583001,583116,583214,583306]
    section7 = [583335,583449]
    sections.append(section1)
    sections.append(section2)
    sections.append(section3)
    sections.append(section4)
    sections.append(section5)
    sections.append(section6)
    sections.append(section7)

    predictors = Dissector(sections, xdata=None, ydata=abp)
    predictors.dissect()
    targets = np.ones(len(predictors))
    return predictors.tolist(), targets.tolist()

def GetA40001TrainingBadSet():
    segmentInto100SampleSets = lambda start,end : \
             [start+i*100 for i in range( int( (end-start)/100 )) ]
    
    sections = []    
    dbPhysionet = DatabasePhysionet()
    record = dbPhysionet.GetRecord('a40001')
    abp = record.p_signals[:,6]
    # Signal dropout starting around 1123500 and ending around 1126760.
    # Refer to img\DropOut_1123500_A40001.png for reference.
    section1 = segmentInto100SampleSets(1123500,1126760)
    # This one might be dubious as the co algorithm might be able to handle this
    # See img\OnePeak_1164490_A40001.png for reference.
    section2 = [1164490,1164500,1164526]
    # Pulse train starting around 1152000 and ending around 1153000.
    # Refer to img\PulseTrain_1152000_A40001.png for reference.
    section3 =  segmentInto100SampleSets(1152000,1153000) 
    # Two heart beats between 569979 and 570193. Partially resolves the issue
    # of sign changes. refer to img\DoubleHeartBeat_569979_570193.png for
    # reference.
    section4 = [569979,570193]
    sections.append(section1)
    sections.append(section2)
    sections.append(section3)
    sections.append(section4)
    
    predictors = Dissector(sections, xdata=None, ydata=abp)
    predictors.dissect()
    targets = np.zeros(len(predictors))
    return predictors.tolist(), targets.tolist()

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    
    def GetFft(data, fftlen=1e3):
        import scipy.fftpack
        # Number of samplepoints
        N = len(data)
        # Pad the data to match the fft length
        padlen = (fftlen-N)/2
        if (fftlen - N) % 2 == 0:
            dataPadded = np.zeros(padlen).tolist() + data.tolist() + np.zeros(padlen).tolist() 
        else:
            dataPadded =  np.zeros(padlen).tolist() + data.tolist() + np.zeros(padlen+1).tolist() 
        # sample spacing
        T = 1.0 / 125.0
        yf = scipy.fftpack.fft(dataPadded)
        xf = np.linspace(0.0, 1.0/(2.0*T), fftlen /2)
        return xf, yf[:fftlen//2]
    
    def PlotOriginalSignal():
        dbPhysionet = DatabasePhysionet()
        record = dbPhysionet.GetRecord('a40001')
        abp = record.p_signals[:,6]
        plt.plot(abp)
        plt.show()
        
    #PlotOriginalSignal()
    print(len(GetA40001TrainingGoodSet()[1]))
    print(len(GetA40001TrainingBadSet()[1]))
    CheckTrainingSetPlots(GetA40001TrainingBadSet()[0])