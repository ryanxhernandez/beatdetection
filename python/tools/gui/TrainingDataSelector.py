'''
Created on Dec 2, 2017

@author: rxher
'''
import wx
from projects.biomed.beat_classification.python.tools.gui.panels.PlottingPanel import PlottingPanel
from projects.biomed.beat_classification.python.tools.gui.panels.FileBrowsingPanel import FileBrowser

class CanvasFrame(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self,None,-1,
                         'SVM Training and Testing Tool',size=(550,350))

        self.SetBackgroundColour(wx.NamedColour("WHITE"))
        
        
        self.hsizer = wx.BoxSizer(wx.HORIZONTAL)
        self.vsizer = wx.BoxSizer(wx.VERTICAL)
        
        self.InitSignalExplorer(self.hsizer)        
        self.InitPlottingPanels(self.vsizer)
        self.hsizer.Add(self.vsizer,1, wx.GROW)
        self.InitTrainingPanels(self.hsizer)
        
        self.SetSizer(self.hsizer)
        self.Fit()
        self.SetInitialSize()
        self.Center()

        
    def InitSignalExplorer(self,sizer):
        self.signalBrowser = FileBrowser(self, wx.ID_ANY)
        sizer.Add(self.signalBrowser,1, wx.GROW)
    
    def InitPlottingPanels(self, sizer):
        self.dataPanel = PlottingPanel(self,wx.ID_ANY,title='Signal')
        self.trainingPanel = PlottingPanel(self,wx.ID_ANY,'Training')
        self.testingPanel = PlottingPanel(self,wx.ID_ANY, 'Testing')
        sizer.Add(self.dataPanel,1, wx.GROW)
        sizer.Add(self.trainingPanel,1, wx.GROW)
        sizer.Add(self.testingPanel,1, wx.GROW)
        
        import numpy as np
        x1 = np.arange(0,100,1)
        x2 = np.sin(x1)
        x = (x1,x2)
        self.dataPanel.SetData(x)
        
    def InitTrainingPanels(self, sizer):
        pass

class App(wx.App):

    def OnInit(self):
        'Create the main window and insert the custom frame'
        frame = CanvasFrame()
        frame.Show(True)

        return True
    
if __name__ =='__main__':
    app = App(0)
    app.MainLoop()
