'''
Created on Dec 3, 2017

@author: rxher
'''

import wx
from matplotlib.figure import Figure

from numpy import arange, sin, pi

import matplotlib

# uncomment the following to use wx rather than wxagg
#matplotlib.use('WX')
#from matplotlib.backends.backend_wx import FigureCanvasWx as FigureCanvas

# comment out the following to use wx rather than wxagg
matplotlib.use('WXAgg')
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas

from matplotlib.backends.backend_wx import NavigationToolbar2Wx

class PlottingPanel(wx.Panel):
    
    def __init__(self, parent, id, title=None):
        wx.Panel.__init__(self, parent,id)
        self.parent = parent
        self.figure = Figure(figsize=(5,3))
        self.axes = self.figure.add_subplot(111)
        if title is not None:
            self.axes.set_title(title)

        self.canvas = FigureCanvas(self, -1, self.figure)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas,  1)
        self.SetSizer(self.sizer)
        self.AddToolbar()  # comment this out for no toolbar
    
    def SetData(self, x):
        if len(x)>2:
            raise ValueError("Plotting Panel does not support plotting more "
                             "than two axis.")
        self.axes.plot(*x)
    
    def AppendData(self):
        pass
    
    def AddToolbar(self):
        self.toolbar = NavigationToolbar2Wx(self.canvas)
        self.toolbar.Realize()
        if wx.Platform == '__WXMAC__':
            # Mac platform (OSX 10.3, MacPython) does not seem to cope with
            # having a toolbar in a sizer. This work-around gets the buttons
            # back, but at the expense of having the toolbar at the top
            self.SetToolBar(self.toolbar)
        else:
            # On Windows platform, default window size is incorrect, so set
            # toolbar width to figure width.
            tw, th = self.toolbar.GetSize()
            fw, fh = self.canvas.GetSize()
            # By adding toolbar in sizer, we are able to put it at the bottom
            # of the frame - so appearance is closer to GTK version.
            # As noted above, doesn't work for Mac.
            self.toolbar.SetSize(wx.Size(fw, th))
            self.sizer.Add(self.toolbar, 0)
        # update the axes menu on the toolbar
        self.toolbar.update()
    
    

    

if __name__ == '__main__':
    pass